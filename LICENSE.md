This font, Agon, is a LAB release from Cubic Type.

LAB releases are experimental in one way or another.

Using the font file is therefore participating in an experiment.
Thank you for your participation.

You are welcome to use the font file, but
you are not licensed to distribute the font file.

I am especially interested to see this font in use.
Please send feedback in the first instance
by electronic mail to drj@pobox.com.
