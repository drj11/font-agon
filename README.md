# Agon

The name _Agon_ is a pun on _agon_ meaning conflict, and
_a -gon_ the suffix meaning angular form.

Agon is created from a sketch (2021-03) starting from my note:
“what if an alphabet, but based on texture and direction“.

The basic elements are a regular hexagon and a lined texture.

In Agon, similar to the regular alphabet,
**O** is typographically foundational.
In Agon the **O** is an empty hexagon.

<figure>
<img src="agon-o.svg"></img>
<figcaption>
O
</figcaption>
</figure>

The hexagon is oriented with
either its flat sides horizontal, which is called _E-form_, or
with its flat sides vertical, which is called _I-form_.
E-form and I-form are named after the letters that use those
hexagon orientations.

<figure>
<img src="agon-ei.svg"></img>
<figcaption>
Agon <b>E</b> on left, and <b>I</b> on right
</figcaption>
</figure>


The inside of a hexagon is either blank
(corresponding to a closed loop in a regular alphabet), or
a lined texture at one of 6 angles
(roughly corresponding to the direction in which a
counter lies open).

Even these simple elements gives rise to 14 combinations, and
they are deployed in multiples of 1 or 2; and exceptionally, in
the case of **M**, 3.
Two letters, **Q** and **X**, break the rules further by
cutting the hexagon form and recombining the pieces.

<figure>
<img src="agon-plaque.svg"></img>
<figcaption>
Agon alphabet: ABCD / EFGH / IJKLMN / OPQRST / UVWXYZ
</figcaption>
</figure>


## Analysis

The font has extremely low comprehension.

Some of the relationships between letters that exist in a
regular alphabet also exist in this alphabet.
For example, **U** is a **V** with a flat bottom;
**W** is **V** + **V**.

<figure>
<img src="agon-uvw.svg"></img>
<figcaption>
U V W
</figcaption>
</figure>

**J** is an **I** with a change of angle.

It can induce feelings of confusion over form that may be
similar to dyslexia or the feeling of learning an unfamiliar
script.
In Agon **D** and **O** are very similar, a blank hexagon.

<figure>
<img src="agon-od.svg"></img>
<figcaption>
O D
</figcaption>
</figure>

But aren't they also quite similar in a regular alphabet?
One is a circle, one is a circle with a flat side.
Perhaps it is only familiarity with alphabets that allows users
to pick out what might seem to others to be minute details.

Some relationships are clearer in Agon than they normally are in
a regular alphabet.
For example in Agon, **A** and **P** share a structure;
**P** is **A** with a rotated texture in the lower hexagon.

<img src="agon-ap.svg"></img>

In a regular font, **A** and **P** do share a structure,
both consist of a closed counter
(the counter is normally triangular in **A** and
semicircular in **P**) above a partly enclosed space.
But this is a structure that we are not normally used to
considering.
Most typographers do not think of **P** as being “a bit like A
but with a different lower space”.

However, in some fonts this structure may be more apparent that in others.
This example shows how in Pump (Letraset, 1970) an **A** can
have its leg truncated to look a little bit like a **P**.

<img src="pump-truncated.svg"></img>

You may enjoy finding your own examples.

# END
